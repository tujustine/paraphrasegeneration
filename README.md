# ParaphraseGeneration

## Data
N'oubliez pas de dézipper le fichier avant de lancer le notebook (prévoyez 4 Go)

## Doc
Ce dossier contient mon rapport

## Script
Ce dossier contient le notebook, le modèle sauvegardé, le png de l'architecture du modèle et les résultats du tuning des hyperparamètres
